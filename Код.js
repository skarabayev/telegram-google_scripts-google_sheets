// В "Файл"->"Свойства проекта"->"Свойства скрипта" создаем две глобальные переменные
// "TELEGRAM_BOT_API_KEY" и "chat_id" (без кавычек)
// и заносим туда данные вашего Telegram бота и чата с ботом соответсвенно
const TELEGRAM_BOT_API_KEY = PropertiesService.getScriptProperties().getProperty('TELEGRAM_BOT_API_KEY');
const chat_id = PropertiesService.getScriptProperties().getProperty('chat_id');
 
function timer() {

  let table = SpreadsheetApp.getActiveSpreadsheet();
  let sheet = table.getSheetByName("список дел");
  
  // Берем данные из таблицы
  // 2 строка, 2 столбец, последня строка-1, 6 столбец
  let data = sheet.getRange(2, 1, sheet.getLastRow(), 2).getValues();
  
  var checkLable = 0; // Ин-я переменной для "умного" выведения заголовка при наличии данных для отправки
  
  // Разбираем данные на отдельные колонки
  for (i = 0; i < (data.length - 1); i++) {
    // Отделяем первую строку
    let dataInfo = data[i]; Logger.log(dataInfo);
    
    let clientName = dataInfo[0]; // Logger.log(clientName); // Отделяем имя клиента
    
    var date = dataInfo[1].toString(); // Logger.log(date); // Отделяем дату
    
    // Фильтруем значния из таблицы относительно текущей даты
    var MILLIS_PER_DAY = 1000 * 60 * 60 * 24; 
    var now = new Date(); // Logger.log(now);
    
    // Сравниваем даты текущую и табличное значение
    if (dataInfo[1].getTime() >= (now.getTime() - MILLIS_PER_DAY) && dataInfo[1].getTime() <= (now.getTime() + 7 * MILLIS_PER_DAY)) // Logger.log("true")
    {
      if (checkLable == 0) {
        sendText(chat_id, "🏛" + " " + "На ближайшую неделю назначены следующие дела:");
        checkLable = 1; 
      }
      sendText(chat_id, clientName + " - " + "заседание суда назначено на " + dataInfo[1].toLocaleDateString());
    }
  }
}

// Функция для отправки сообщений в телеграмм
function sendText(chat_id, text, keyBoard) {

  let data = {
    method: 'post',
    payload: {
      method: 'sendMessage',
      chat_id: String(chat_id),
      text: text,
      parse_mode: 'HTML',
      reply_markup: JSON.stringify(keyBoard)
    }
  }
  
  UrlFetchApp.fetch('https://api.telegram.org/bot' + TELEGRAM_BOT_API_KEY + '/', data);
}


// Функция для работы с интерфейсом таблиц
function onOpen() {
  Logger.log(typeof TELEGRAM_BOT_API_KEY + "\n" + typeof chat_id);
    if (TELEGRAM_BOT_API_KEY == null) {
      showPrompt("TELEGRAM_BOT_API_KEY")
    }
    if (chat_id == null) {
      showPrompt("chat_id")
    }
}

// Функция для ввода данных токена Telegram
// Ист-к.: https://codd-wd.ru/shpargalka-dopolneniya-dlya-google-tablic-spreadsheets-i-ispolzovanie-google-apps-script/#p9
function showPrompt(argument) {
  var ui = SpreadsheetApp.getUi();
 
  // https://developers.google.com/apps-script/reference/base/ui#prompttitle-prompt-buttons
  var result = ui.prompt(
      "Настройка работы с telegram", // Заголовок
      "Для работы с telegram необходимо указать значение TELEGRAM_BOT_API_KEY и chat_id" + "\n" +
      "Введите " + argument + ":", // Сообщение
      ui.ButtonSet.OK_CANCEL // Кнопки
  );
 
  var button = result.getSelectedButton(); // Кнопка, на которую нажал пользователь
  var prompt_result = result.getResponseText(); // Текст, который ввёл пользователь

  if (button == ui.Button.OK) {
    ScriptProperties.setProperty(argument, prompt_result);
    ui.alert("Значение переменной " + argument + ":" + prompt_result + "," + "\n" + "успешно сохранен в глобальную переменную");
  } else if (button == ui.Button.CANCEL) {
    ui.alert("Ввод данных отменен." + "\n" + "Для повторения настройки перезапустите страницу браузера.");
  } else if (button == ui.Button.CLOSE) {
    ui.alert("Диалоговое окно настройки закрыто принудительно." + "\n" + "Для повторения настройки перезапустите страницу браузера.");
  }
  
}